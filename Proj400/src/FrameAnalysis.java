/*
 * NEED TO ASSIGN EACH INDIVIDUAL COLOR (0 to 16,777,215) TO ONE OF THE Notes 
 * 
 * GRAYSCALE CONVERSION OBTAINED FROM:
 * http://www.tutorialspoint.com/java_dip/grayscale_conversion.htm
 * 
 */

//import java.awt.*;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

public class FrameAnalysis 
{	
	public void getMotion(int[][] arr_TopTenColors, BufferedImage[] frameArray, float[] comparisons, int numAnalyzing, int pixWidth, int pixHeight, String files)	//constructor (used to pass arguments to this thread) 
	{
/* *///	int framestoAnalyze = numAnalyzing;
		int framestoAnalyze = numAnalyzing;
		String fileLocation = files;
		float matchPercent  = 0;
		
		//PIXELS
		  int picWidth  = pixWidth;
		  int picHeight = pixHeight;
		  int totPixels = pixWidth * pixHeight;
		  int pix1;
		  int pix2;
		  int pixCM = 0; //PIXEL COMPARISON MATCH - USED IN COMPARISON ALGORITHM
		  int maxColor = 16777216;
		//PIXELS
		
		  @SuppressWarnings("unused")
		int pwalk = 0;
		  int multi = 0;
		  
		  
		//IMAGES BEING COMPARED
		  BufferedImage imgComp1 = null;
		  BufferedImage imgComp2 = null;
		//IMAGES BEING COMPARED		
		
		//ARRAYS & ARRAYLISTS  
			int[] allColors1 = new int[maxColor];
			int[] allColors2 = new int[maxColor];
			
			ArrayList<Integer> p1_AListIndex = new ArrayList<Integer>();
			ArrayList<Integer> p1_AListValue = new ArrayList<Integer>();
			
			ArrayList<Integer> p2_AListIndex = new ArrayList<Integer>();
			ArrayList<Integer> p2_AListValue = new ArrayList<Integer>();
			
			p1_AListIndex.ensureCapacity(totPixels);
			p1_AListValue.ensureCapacity(totPixels);
			p2_AListIndex.ensureCapacity(totPixels);
			p2_AListValue.ensureCapacity(totPixels);
			
			int small = 10;
			int[] numOccurences = new int[small];
			int[] index = new int[small];

			for(int i =0; i<small; i++)
				{	numOccurences[i] = 0;
					index[i] = 0;		}
			
			for(int i =0; i<maxColor; i++)
				{	allColors1[i] = 128;
					allColors2[i] = 128;		}
		//ARRAYS & ARRAYLISTS
		
/**/	int advPic0 = 0;
		int advPic1 = 1;
		int mypoint=0;
		int point = 0;
			//System.out.println("Picture " + advPic1);
			while((advPic0 < framestoAnalyze) && (advPic1 < framestoAnalyze)) //WHILE THERE ARE FRAMES TO ANALYZE
			{
				try {	imgComp1 = ImageIO.read(new File(fileLocation + "ColorImage" + advPic0 + ".jpg"));	} 
				catch (IOException e1) { e1.printStackTrace();	} 
				
				try {	imgComp2 = ImageIO.read(new File(fileLocation + "ColorImage" + advPic1 + ".jpg"));	} 
				catch (IOException e1) { e1.printStackTrace();	} 
								
				for (int y = 0; y < picHeight; y++)		//\\\\\\
			    {											  //TRAVERSE PIXELS	
			    	for (int x = 0; x < picWidth; x++)  ////////
			    	{
			    	//PIXEL COLOR VALUES  PIXEL COLOR VALUES	
			    	//GET PIXEL COLOR VALUE 
			       		pix1 = (Math.abs(pix1=imgComp1.getRGB(x, y))-1);
			       		pix2 = (Math.abs(pix2=imgComp2.getRGB(x, y))-1);			       		
			    	//GET PIXEL COLOR VALUE
//					
//					//THIS LINE SHOWS PIXELS BEING COMPARED FOR PIC1 & PIC2			       		
//			       		System.out.println(pix1 + "   ||   " + pix2);
//					//THIS LINE SHOWS PIXELS BEING COMPARED FOR PIC1 & PIC2
//			       		
			       	//STORE COLOR DATA FOR INDIVIDUAL PIXELS
			       		allColors1[pix1] = allColors1[pix1] + 1;
			       		allColors2[pix2] = allColors2[pix2] + 1;
			       	//STORE COLOR DATA FOR INDIVIDUAL PIXELS
			       	//PIXEL COLOR VALUES  PIXEL COLOR VALUES
//
//			       		
			       		//CONVERT pic1(pix1)TO GRAYSCALE
			               Color p1 = new Color(pix1);
			               int red1 = (int)(p1.getRed() * 0.299);
			               int green1 = (int)(p1.getGreen() * 0.587);
			               int blue1 = (int)(p1.getBlue() *0.114);
			               Color newColorpic1 = new Color(red1+green1+blue1,red1+green1+blue1,red1+green1+blue1);
			               imgComp1.setRGB(x,y,newColorpic1.getRGB());
			             //CONVERT pic1(pix1)TO GRAYSCALE
//		               
				       	 //CONVERT pic2(pix2)TO GRAYSCALE
			               Color p2 = new Color(pix2);
			               int red2 = (int)(p2.getRed() * 0.299);
			               int green2 = (int)(p2.getGreen() * 0.587);
			               int blue2 = (int)(p2.getBlue() *0.114);
			               Color newColorpic2 = new Color(red2+green2+blue2,red2+green2+blue2,red2+green2+blue2);
			               imgComp2.setRGB(x,y,newColorpic2.getRGB());
			             //CONVERT pic2(pix2)TO GRAYSCALE
//		               
			             //GET PIXEL GRAYSCALE VALUE 
				    	   pix1=imgComp1.getRGB(x, y);
				       	   pix2=imgComp2.getRGB(x, y);
				         //GET PIXEL GRAYSCALE VALUE
//
//
				       	 //DETERMINE MOTION (ALLOWING FOR RANGE OF 14)  
/***/					       	int diff = 1;				       	   			
/***/					      	if ((pix2 > pix1 - diff) && (pix2 < pix1 + diff))
/***/					      	pixCM += 1;	
				      	//DETERMINE MOTION (ALLOWING FOR RANGE OF 14)
			    	}//END FOR -- WIDTH
			    }//END FOR -- HEIGHT
				
				
				if (advPic0 % 10 == 0)
						{	System.out.println("Pic " + advPic0 + " AND Pic " + advPic1 + " were converted to grayscale.");	}
//
//
//			
				//COMPARE FRAMES
				  matchPercent = ((float)pixCM /((float)totPixels))*100;
				  comparisons[point] = matchPercent;				  
				  matchPercent = 0;
				  pixCM = 0;
				  point += 1;
				//COMPARE FRAMES
//				

//OBTAIN 10 MOST USED COLORS PER FRAME
//				  
//THIS SECTION GOES THROUGH THE 16,777,216 INDEX ARRAYS FOR PIC 1 AND PIC 2				  			
//IT GETS THE MOST USED COLOR VALUE AND STORE IT IN A MUCH SMALLER ARRAY			  			
//THE ARRAY INDEXES WITH NON-ZERO VALUES ARE STORED IN AN ARRAY LIST
//  				
	
	//VALUES FROM allColors1[] && allColors2[] placed into arraylists //BOTH FRAMES
		for(int w1 = 0; w1 < maxColor; w1++)
		{	if (allColors1[w1] != 128)
			{	p1_AListIndex.add(w1);
				p1_AListValue.add(allColors1[w1]);	}/*END IF*/
			if (allColors2[w1] != 128)
			{	p2_AListIndex.add(w1);
				p2_AListValue.add(allColors1[w1]);		
				pwalk++;							}/*END IF*/ 	
		}/*END FOR*/
	//VALUES FROM allColors1[] && allColors2[] placed into arraylists //BOTH FRAMES
			
/***/		//System.out.println("Arraylist should be much shorter but = " + p2walk);
						  	
	//OBTAIN 10 MOST USED COLORS AND PLACE INTO ARRAY topTenColors[][]
		//FRAME 1  FRAME 1  FRAME 1  FRAME 1  FRAME 1
		

			//System.out.println("Array List Size = " +p1_AListIndex.size());
		
		//FRAME 1
		//FEWER THAN 10 COLORS IN FRAME
		if(p1_AListIndex.size()<10)
		{
			if (p1_AListIndex.size() == 1)
			{
				for(int tiny = 0; tiny < p1_AListIndex.size(); tiny++)
				{
					numOccurences[tiny] = p1_AListValue.get(tiny);
					index[tiny] = p1_AListIndex.get(tiny);
				}
			}
			else
				multi = 0;
				for(int tiny = 0; tiny < p1_AListIndex.size(); tiny++)
				{
					
					if(tiny % p1_AListIndex.size() == 0)
						{	multi = multi + 1;	}
					numOccurences[tiny] = p1_AListValue.get(tiny + (tiny * multi));
					index[tiny] = p1_AListIndex.get(tiny+ (tiny * multi));
					System.out.println("Multi = " + multi);
				}
				
			//System.out.print("Fewer than 10 Colors in frame... ");
		}
		//FEWER THAN 10 COLORS IN FRAME
		
		//GREATER THAN 10 COLORS IN FRAME
		//FRAME 1
		else if(p1_AListIndex.size() > small)	//IF ARRAYLIST SIZE IS GREATER THAN 10...
		{
			System.out.println("The following number MUST be less than " + totPixels);
			System.out.println( p1_AListIndex.size());
			
			for(int bigwalk = 0; bigwalk < p1_AListIndex.size(); bigwalk++ )	//STEP THROUGH EACH INDEX
			{					
				for(int mypointer = 0; mypointer < small; mypointer++)	//STEP THROUGH INDEXES HOLDING 10 CURRENT LARGEST VALUES
				{
					if(p1_AListValue.get(bigwalk) > numOccurences[mypoint]) 	//IS VALUE GREATER THAN ANY VALUE IN INDEX
					{
						mypoint = mypointer;	//GET INDEX
						
						//INSERT THE NUMBER PUSHING SMALLER VALUES 
						for(int gotcha = small-1; gotcha > mypoint; gotcha--)
						{
							numOccurences[gotcha] = numOccurences[gotcha-1];
							index[gotcha] = index[gotcha-1];
						}
						numOccurences[mypoint] = p1_AListValue.get(bigwalk);
						index[mypoint] = p1_AListIndex.get(bigwalk);
						mypointer = small;		//SET mypointer TO VALUE ALLOWING PROGRAM TO LEAVE FOR LOOP
					}
				}
			}				
		}
		for(int i = 0; i < small; i++)
		{
			arr_TopTenColors[advPic0][i] = index[i];
		}
		//GREATER THAN 10 COLORS IN FRAME
			//FRAME 1 COMPLETED  FRAME 1 COMPLETED  FRAME 1 COMPLETED  FRAME 1 COMPLETED
//		
//		
//	
		//FRAME 2
		//FEWER THAN 10 COLORS IN FRAME
		if(p1_AListIndex.size()<10)
		{
			if (p2_AListIndex.size() == 1)
			{
				for(int tiny = 0; tiny < p2_AListIndex.size(); tiny++)
				{
					numOccurences[tiny] = p2_AListValue.get(tiny);
					index[tiny] = p2_AListIndex.get(tiny);
				}
			}
			else
				multi = 0;
				for(int tiny = 0; tiny < p2_AListIndex.size(); tiny++)
				{
					
					if(tiny % p2_AListIndex.size() == 0)
						{	multi = multi + 1;	}
					numOccurences[tiny] = p2_AListValue.get(tiny + (tiny * multi));
					index[tiny] = p2_AListIndex.get(tiny+ (tiny * multi));
					System.out.println("Multi = " + multi);
				}
				
			//System.out.print("Fewer than 10 Colors in frame... ");
		}
		//FRAME 2
		//FEWER THAN 10 COLORS IN FRAME

		//GREATER THAN 10 COLORS IN FRAME
		//FRAME 2
		else if(p2_AListIndex.size() > small)	//IF ARRAYLIST SIZE IS GREATER THAN 10...
		{
			for(int bigwalk = 0; bigwalk < p2_AListIndex.size(); bigwalk++ )	//STEP THROUGH EACH INDEX
			{					
				for(int mypointer = 0; mypointer < small; mypointer++)	
				{
					if(p2_AListValue.get(bigwalk) > numOccurences[mypoint]) 	//VALUE GREATER THAN ANY SAMLL INDEX?
					{
						mypoint = mypointer;	//GET INDEX
						
						
						for(int gotcha = small-1; gotcha > mypoint; gotcha--)
						{
							numOccurences[gotcha] = numOccurences[gotcha-1];
							index[gotcha] = index[gotcha-1];
						}
						numOccurences[mypoint] = p2_AListValue.get(bigwalk);
						index[mypoint] = p2_AListIndex.get(bigwalk);
						mypointer = small;		//SET TO LEAVE FOR LOOP
					}
				}
			}				
		}
			
		for(int i = 0; i < small; i++)
		{
			arr_TopTenColors[advPic1][i] = index[i];
		}
		
		int i = 0;
		
	//**END **// OBTAIN 10 MOST USED COLORS PER FRAME //**END **// 
				  
			  
				  
				  
/***/				  	//for(int test=0; test<16777216; test++)
/***/				  	//{	System.out.println(allColors1[test]);	}
				  
				  	//RE-INITIALIZE VAR VALUES
					advPic0 += 2;
					advPic1 += 2;
					for( i =0; i<10; i++)
					{		
						numOccurences[i] = 0;
						index[i] = 0;		
					}
					for( i =0; i<maxColor; i++)
					{		
						allColors1[i] = 0;
						allColors2[i] = 0;		
					}
					
					p1_AListIndex.clear();
					p1_AListValue.clear();
					p2_AListIndex.clear();
					p2_AListValue.clear();
					//RE-INITIALIZE VAR VALUES
					
			//END WHILE LOOP -- WHILE FRAMES TO ANALYZE...
}

			for(int replay = 0; replay < framestoAnalyze; replay++)
			{
				System.out.println(comparisons[replay/2] + "% match");
			}			
	}//END METHOD Frame_analysis_mod_equ0
}//END public class Frame_analysis