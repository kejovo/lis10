/*
 * ON_STARTUP
 * 
 * OPENS A CSV FILE CONTAINING 2 COLUMNS 
 * COLUMN 1 = INTEGER EQUIVALENT OF A COLOR
 * COLUMN 2 = MIDI EQUIVALENT OF A NOTE
 * 
 * THE NUMBERS IN COLUMN 1 ACT AS A RANGE
 * THE RANGE FROM THE FIRST NUMBER TO THE SECOND
 * ARE USED TO PLACE THE MIDI EQUIVALENT VALUE
 * INTO THE BYTE[] MIDI_EQUIV
 * 
 * EXAMPLE USING LINES 2 & 3:
 * LINE 2: 16777190,4
 * lINE 3: 16776166,3
 * 
 * MIDI_EQUIV[16777190] ALL THE WAY THROUGH MIDI_EQUIV[16776166]
 * WILL STORE THE VALUE 3 (A NOTE ON THE MIDI SCALE WHICH RANGES (0 - 127)
 * 
 */

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

	public class OnStartUp 
	{
		public void Populate_ColorVals(String fileLocation, String csvFileName, byte[] midiEquiv) 
		{
			String path = fileLocation;
			String file = csvFileName;
			String csvFile = path + file;
			
//THIS COMMENTED SECTION IS SOMETHING NEW TO TRY TO OBTAIN A MORE DIVERSE
//RANGE OF SOUNDS.  THIS WILL BE WORKED ON POST SEMESTER AS IT IS VERY 
//NERVE WRACKING TO ATTEMPT TO MAKE LARGE SCALE CHANGES SO LATE IN THE 
//SEMESTER			
			int[] Octave01 = {0,4,3,1,11,2,10,7,6,5,9,8};
			int[] Octave02 = {12,16,15,13,23,14,22,19,18,17,21,20};
			int[] Octave03 = {24,28,27,25,35,26,34,31,30,29,33,32};
			int[] Octave04 = {36,40,39,37,47,38,46,43,42,41,45,44};
			int[] Octave05 = {48,52,51,49,59,50,58,55,54,53,57,56};
			int[] Octave06 = {60,64,63,61,71,62,70,67,66,65,69,68};
			int[] Octave07 = {72,76,75,73,83,74,82,79,78,77,81,80};
			int[] Octave08 = {84,88,87,85,95,86,94,91,90,89,93,92};
			int[] Octave09 = {96,100,99,97,107,98,106,103,102,101,105,104};
			int[] Octave10 = {108,112,111,109,119,110,118,115,114,113,117,116};
			
			int numColors   = 16777216;
			int sectionSize = 1677720;
			int walk = 0;
			int i = 0;
			int multi = 0;
			
			
			for (walk = 0; walk < numColors; walk++)
			{
				if (walk % sectionSize == 0 && walk != 0)
					{	multi = multi + 1;	}
				
					if(i == 12)
						{	i = 0;	}
					if (i != 12)
					{
						switch(multi)
						{    
						    case 0:
						    	midiEquiv[walk] = (byte) Octave01[i];	
								i++;
						        break;
						    case 1:
						    	midiEquiv[walk] = (byte) Octave02[i];	
								i++;
						        break;
						    case 2:
						    	midiEquiv[walk] = (byte) Octave03[i];	
								i++;
						        break;
						    case 3:
						    	midiEquiv[walk] = (byte) Octave04[i];	
								i++;	
						        break;
						    case 4:
						    	midiEquiv[walk] = (byte) Octave05[i];	
								i++;
						        break;
						    case 5:
						    	midiEquiv[walk] = (byte) Octave06[i];	
								i++;
						        break;
						    case 6:
						    	midiEquiv[walk] = (byte) Octave07[i];	
								i++;
						        break;
						    case 7:
						    	midiEquiv[walk] = (byte) Octave08[i];	
								i++;
						        break;
						    case 8:
						    	midiEquiv[walk] = (byte) Octave09[i];	
								i++;
						        break;
						    case 9:
						    	midiEquiv[walk] = (byte) Octave10[i];	
								i++;
						        break;
						    default:						        
						        break;
						}
						
					}
					
				}
				
				
			
					
/*			for (int j = 0; j< numColors; j++)
			{
				for(int k = 0; k < 12; k ++)
					{	System.out.println(midiEquiv[j+k]);	}
				
				j += sectionSize;
				System.out.println("Index # " + j);	
			}*/
/*		//INITIALIZE VARIABLES
			BufferedReader br = null;
			String line = "";
			String cvsSplitBy = ",";

			int across = 0;
			int down = 0;
			int count = 0;
			
			int[][] storeNotes = new int[120][2];
		//INITIALIZE VARIABLES
			
		//TRY OPENING FILE
			try {

				br = new BufferedReader(new FileReader(csvFile));
				while ((line = br.readLine()) != null) 
				{
		//TRY OPENING FILE
		
				// use comma as separator
					String[] Notes = line.split(cvsSplitBy);
					
					storeNotes[across][down] = Integer.parseInt(Notes[0]);
					storeNotes[across][down+1] = Integer.parseInt(Notes[1]);
					
					down = 0;
					across += 1;
					count += 1;					
				}//END WHILE
				down = 0;

			} 
			catch (FileNotFoundException e) 
				{	e.printStackTrace();	} 
			catch (IOException e) 
				{	e.printStackTrace();	} 
			finally 
				{
				if (br != null) 
				{	try {	br.close();	} 
						catch (IOException e) {	e.printStackTrace();	}
				}
			}			
			
			System.out.println("Done");

			down = 0;			
			int start = 0;
			int finish = 16777215;
			int midiNote = 0;
			
			for(int i = 0; i<count; i++)
			{
				start = (storeNotes[i][down]);
				finish = (storeNotes[i+1][down]);
				midiNote = (storeNotes[i][down+1]);
				
				if(midiNote != 0 && midiNote%12 !=0)
				{
					for(int step = start; step >= finish; step--)
					{
						midiEquiv[step] = (byte) midiNote;
					}
				}
				else
				{
					start = ((storeNotes[i][down]));
					finish = (storeNotes[i][down]);
					midiNote = (storeNotes[i][down+1]);
					//System.out.println("Ranges From " + start + " to " + finish + " and storing the range in " +  midiNote + ".");
					midiEquiv[finish] = (byte) midiNote;
					
					start = ((storeNotes[i][down])-1);
					finish = (storeNotes[i+1][down]);
					midiNote = (storeNotes[i+1][down+1]);
					
					for(int step = start; step >= finish; step--)
					{
						midiEquiv[step] = (byte) midiNote;
					}
					//System.out.println("Ranges From " + start + " to " + finish + " and storing the range in " +  midiNote + ".");
				}	
			}//END FOR 
*/		}//END METHOD On_StartUp
	}//END CLASS On_StartUp