import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Synthesizer;

public class Play_thatFunkyMusic 
{
	public void Mmm_DoIt(String fileLocation, String videoName, float[] arr_NoteLength, int[] arr_NotePitch, int[][] arr_TopTenColors, String csvFileName, byte[] midiEquiv, int seconds)
	{
		
	// note = 60;
	int volume = 127;
	float noteLength;
		
	//CREATE FILE
	String filename = "MIDI"+videoName;
	String storeMIDIData="";
	File saveMIDItoFile = new File(fileLocation, filename + ".txt");
	//CREATE FILE
	
	int protect = 1;

	int sixteenth = 063; //FOR LATER USE
	int eighth = 125;
	int quarter = 250;
	int half = 500;
	int quarter3 = 750;
	int whole = 1000;
	//int background = 2000; //FOR LATER USE
	
	int noteNum = 0;	
	int noteDuration=0;
	//NOTE DURATION	
	
    try {
    	
    	//Desktop.getDesktop().open(new File(fileLocation + videoName));

    	//MIDI STARTUP
	    	Synthesizer synthesizer = MidiSystem.getSynthesizer();
	        synthesizer.open();
	        MidiChannel[] channels = synthesizer.getChannels();
        //MIDI STARTUP
        
        
	for(int i = 0; i < seconds; i++)//FOR EACH SECOND...
	{
//		channels[0].noteOn(90, 50);
//        Thread.sleep(500);
//        channels[0].noteOff(90);
		
		//DETERMINE NOTE LENGTH FOR THE SECOND
		//DETERMINE NUMBER OF NOTES PER SECOND
		noteLength = 100 - arr_NoteLength[i];
		if (noteLength <=6.25)
		{	noteLength = sixteenth;	
			noteNum = 16;			}
		else if (noteLength >= 0 && noteLength <= 12.5)
		{	noteLength = eighth;	
			noteNum = 8;			}
		else if (noteLength >12.5 && noteLength <= 37.5)
		{	noteLength = quarter;	
			noteNum = 4;			}
		else if (noteLength >37.5 && noteLength <= 62.5)
		{	noteLength = half;	
			noteNum = 2;			}
		else if (noteLength >62.5 && noteLength <= 87.5)
		{	noteLength = quarter3;	
			noteNum = 1;			}
		else if (noteLength >87.5 && noteLength <= 100.0)
		{	noteLength = whole;	
			noteNum = 1;			}

	//GRAB THE NOTES
		for(int noteGrab = 0; noteGrab < noteNum; noteGrab++)
		{	
			//NOTE 1
			protect = noteGrab % 10;
			arr_NotePitch[noteGrab] = arr_TopTenColors[i][protect];
			storeMIDIData = (storeMIDIData + ("channels[0].noteOn(" + arr_NotePitch[noteGrab] + ", " + volume+ ");"));
			
			
			//NOTE 2
			protect = ((protect + 1) % 10);
			arr_NotePitch[noteGrab] = arr_TopTenColors[i][protect];
			storeMIDIData = (storeMIDIData + ("channels[0].noteOn(" + arr_NotePitch[noteGrab] + ", " + volume+ ");"));
			storeMIDIData = (storeMIDIData + ("Thread.sleep(" + (int) noteLength + ");"));
			
		//QUARTER NOTE PAUSE
			if(noteLength == quarter3)
			{
				storeMIDIData = (storeMIDIData + ("channels[0].noteOn(" + -1 + ", " + volume+ ");"));
				storeMIDIData = (storeMIDIData + ("Thread.sleep(" + (int) quarter + ");"));
				storeMIDIData = (storeMIDIData + "channels[0].noteOff(-1);");
			}
		//QUARTER NOTE PAUSE
			storeMIDIData = (storeMIDIData + "channels[0].noteOff(" + arr_NotePitch[noteGrab] + ");");
		}
		
	//GRAB THE NOTES

		
//
    }//END FOR
    synthesizer.close();
    }//END TRY
    catch (Exception e)	{	e.printStackTrace();	}
    BufferedWriter writer = null;// = null;
    System.out.println("Attempting to write to file");
    try 
    {	writer = new BufferedWriter(new FileWriter(saveMIDItoFile));
		writer.write(storeMIDIData);					} 
    	catch (FileNotFoundException e) {	e.printStackTrace();	} 
    	catch (IOException e) {	e.printStackTrace();	}
    try {	writer.close();	} 
    	catch (IOException e) {	e.printStackTrace();	}
	}
}