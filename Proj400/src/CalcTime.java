/*
 * calcTime: 	used to obtain run time of various components
 */
public class CalcTime
{
	public void determineTime(double initTime)
	{
		double startTime = initTime;
		double stopTime = System.currentTimeMillis();
		double estimatedTime = stopTime - startTime;
		//System.out.println(startTime);
		//System.out.println(stopTime);

		//Threading_imageAnalysis
		//System.out.println(estimatedTime);

		//System.out.println(estimatedTime);
		//pixelData
		int minutes;
		int seconds;
		double mintime = (estimatedTime/1000)/60;
		double sectime = (estimatedTime/1000)%60;
		
		System.out.println(" \n\nObtaining Time to Run... ");
		System.out.println("mintime " + mintime);		
		System.out.println("sectime " + sectime);
		
		minutes=(int)mintime;
		seconds=(int)sectime;		
		System.out.print("That took " + minutes + " Minutes " + seconds + " seconds.");
		
	}
}
