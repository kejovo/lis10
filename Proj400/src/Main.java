import java.awt.image.BufferedImage;
import java.io.IOException;

import org.bytedeco.javacv.FrameGrabber.Exception;

/*
 * Name:	CSC400 LIS^10
 * by: 		Kenneth Voccola
 * 
 * External .jar files Required for this Application: ffmpeg.jar; javacv.jar; jna-4.2.1.jar; opencv-windows-x86_64.jar
 * 
 * Last update: 14MARCH2016	
 * 
 */

public class Main 
{
	public static void main(String[] args) throws Exception, IOException 
	{	
	  //fileLocation will be where I place the video for testing
	  //021Hectic.mp4 2 mins 04sec = 124 sec @ 29fps = 3596 frames	//45secs @29fps = 1305 frames
		//That took 8 Minutes 41 seconds.
	  
		//021Relax.mp4  1 mins 14sec =  74 sec @ 30fps = 2220 frames	//45secs @30fps = 1350 frames
		//That took 9 Minutes 57 seconds.
		
		//021Gage.MOV   1 mins 41sec = 101 sec @ 23fps = 2323 frames	//45secs @23fps = 1035 frames
		//That took 13 Minutes 1 seconds.
		
		// Calm Seas.mp4  10sec @ 29fps		
		//Random Color Template.mp4 26sec @ 30fps		
		//BRIGHT FLASHING COLORS!.mp4 19sec @29fps

		String fileLocation = "C:\\Users\\kejovo\\Desktop\\imagesFromVideo\\";
		String videoName = "Calm Seas.mp4"; // 021Hectic.mp4   021Relax.mp4   021Gage.MOV
		String csvFileName = "021MIDI_Equiv.csv"; //THIS FILE ASSIGNS A NOTE TO EACH OF THE 16777216 COLORS
		
		
		int fps = 29;
		int seconds = 10;
		int framesOverkill = fps*seconds;  		//320,000... More frames than a full length feature film @ 48 fps
		int framesTotal = 320000;		  	//Variable stores TOTAL frame number in video
		
		int pixHeight;
		int pixWidth;
		
		long startTime = System.currentTimeMillis();
//		System.out.println("Total Frames =" + framesTotal);
//
		//ASSIGN NOTES TO COLORS
			byte[] arr_MIDIEquiv = new byte[16777216];	
			OnStartUp call_PCV = new OnStartUp();
			call_PCV.Populate_ColorVals(fileLocation, csvFileName, arr_MIDIEquiv);
    	//ASSIGN NOTES TO COLORS
//			
		//OBTAIN TOTAL FRAME COUNT FOR VIDEO	  	  	
			FrameData VidFrames = new FrameData();
			framesTotal = VidFrames.gf_count(framesOverkill, fileLocation, videoName);
			System.out.println("Total Frames = " + framesTotal);
		//OBTAIN TOTAL FRAME COUNT FOR VIDEO
// 				
	 	//DECLARE NECESSARY ARRAYS DECLARE NECESSARY ARRAYS DECLARE NECESSARY ARRAYS DECLARE NECESSARY ARRAYS DECLARE NECESSARY ARRAYS 
			BufferedImage[] frameArray = new BufferedImage[framesTotal]; //ARRAY TO STORE IMAGES FOR ANALYSIS
			float[] arr_Comparisons = new float[(framesTotal/2)+1]; //COMPARISONS ARE MADE WITH NEW FRAMES ONLY --NOT ROLLING FRAMES...//I.E. COMPARE 1 & 2; 3 & 4; 5 & 6...  NOT COMAPARE 1 & 2; 2 & 3; 3 & 4...
			int[][] arr_TopTenColors = new int[framesTotal][10];	//TOP TEN COLORS FOR EACH FRAME
			float[] arr_NoteLength = new float[seconds];			//NOTE DURATION
			int[] arr_NotePitch = new int[fps];							//NOTE
		//DECLARE NECESSARY ARRAYS DECLARE NECESSARY ARRAYS DECLARE NECESSARY ARRAYS DECLARE NECESSARY ARRAYS DECLARE NECESSARY ARRAYS 	
//				
		//OBTAIN PIXEL DATA || HEIGHT/WIDTH   OBTAIN PIXEL DATA || HEIGHT/WIDTH   OBTAIN PIXEL DATA || HEIGHT/WIDTH    			
	 		FrameData PixelWidth = new FrameData();
	 		pixWidth = PixelWidth.gpWidth(fileLocation, videoName);
	 		
	 		FrameData PixelHeight = new FrameData();
	 		pixHeight = PixelHeight.gpHeight(fileLocation, videoName);
		    
	 		System.out.println(pixWidth + " * " + pixHeight + " = " + pixWidth*pixHeight + " pixels"); 		
	 	//OBTAIN PIXEL DATA || HEIGHT/WIDTH   OBTAIN PIXEL DATA || HEIGHT/WIDTH   OBTAIN PIXEL DATA || HEIGHT/WIDTH    		
// 		
	 	//DETERMINE MOTION BETWEEN FRAMES && DETERMINE TOP 10 COLORS PER FRAME   				
		if(framesTotal > framesOverkill)
			{	framesTotal = framesOverkill;	}
	 		System.out.println("frames total =" + framesTotal);
	 		FrameAnalysis call_GM = new FrameAnalysis();
			call_GM.getMotion(arr_TopTenColors, frameArray, arr_Comparisons, framesTotal, pixWidth, pixHeight, fileLocation); //send buffered image array  
		//DETERMINE MOTION BETWEEN FRAMES && DETERMINE TOP 10 COLORS PER FRAME    	
//
		//CONVERT DATA TO MIDI INPUT VALUES
			DataConversion GenMIDIdata = new DataConversion();
			GenMIDIdata.toMIDI(arr_NoteLength, arr_TopTenColors,arr_Comparisons,arr_MIDIEquiv, fps, framesTotal, seconds);
		//CONVERT DATA TO MIDI INPUT VALUES	
//			
		//PLAY SOME SOUNDS
			Play_thatFunkyMusic playSound = new Play_thatFunkyMusic();
			playSound.Mmm_DoIt(fileLocation, videoName, arr_NoteLength, arr_NotePitch, arr_TopTenColors, csvFileName, arr_MIDIEquiv, seconds);
		//PLAY SOME SOUNDS	
//			
		//CALL METHOD determineTime in CLASS calcTime
			CalcTime time = new CalcTime();
			time.determineTime(startTime);
		//CALL METHOD determineTime in CLASS calcTime
//			
	System.out.println("  Total Run Time.");
	}
}