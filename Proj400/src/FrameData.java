import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.FrameGrabber.Exception;

public class FrameData 
{	
	int count = 0;
	int picWidth;
	int picHeight;

	BufferedImage img = null;
	
	//GET FRAME COUNT
	public int gf_count(int numofFrames, String fileLocationsent, String videoNamesent) throws IOException
	{		 
		String fileLocation = fileLocationsent;
		String videoName = videoNamesent;
//		
		int frameNums = numofFrames;
		int totFrames = 0;
		int num = 0;
		//BufferedImage[] frameArray = new BufferedImage[frameNums];
		System.out.println("Determining # of Frames in Video...  Please be patient.");
		FFmpegFrameGrabber grab = new FFmpegFrameGrabber(fileLocation + videoName);

			try {	grab.start(); } 
			catch (Exception e) { 	System.out.println("Unable to grab frames");  }
			
			double startTime = System.currentTimeMillis();	

			for(int i = 0 ; i < frameNums  ; i++) 
			{
				num = i;
			    try { img = grab.grab().getBufferedImage(); } 
			    catch (NullPointerException | Exception e1) 
			    {	i = frameNums;	}			    
					
			    	//SAVE IMAGE TO FOLDER
					  File outputfile = new File(fileLocation + "ColorImage" + num + ".jpg");
					  try {	ImageIO.write(img, "jpg", outputfile);	} 
					  catch (IOException e) { 	/*e.printStackTrace()*/;	}
					//SAVE IMAGE TO FOLDER
				if(i % 100 == 0) 
				{	System.out.println(i);}
				
				totFrames = num;				
			}//END for

			//CALL METHOD determineTime in CLASS calcTime
			CalcTime time = new CalcTime();
			time.determineTime(startTime);
			//CALL METHOD determineTime in CLASS calcTime
			
			System.out.println("  TO COUNT " + totFrames + " FRAMES");
		
			try {	grab.stop();	} 
			catch (Exception f) {	System.out.println("Unable to Stop Grabbing Frames/Close");	} 
			
			System.out.println("Number of frames Determined... " + totFrames + " frames");
			
		return totFrames;
		
		}//END METHOD long getFrameCount()
//
		//GET PIXELS WIDTH
		public int gpWidth(String fileLocationsent, String videoNamesent)
		{
			int picWidth;
			String fileLocation = fileLocationsent;
			String videoName = videoNamesent;
			BufferedImage imgWdth = null;
			
			FFmpegFrameGrabber grab = new FFmpegFrameGrabber(fileLocation + videoName);
			try {	grab.start(); 	} 
			catch (Exception e) {	System.out.println("Unable to grab frames");	}
			
			try {	imgWdth= grab.grab().getBufferedImage();	} 
			catch (org.bytedeco.javacv.FrameGrabber.Exception e) {	e.printStackTrace(); }
				    	
			picWidth = imgWdth.getWidth();  
					    
			return picWidth;			
		}//END GET PIXELS WIDTH	
//
		//GET PIXELS HEIGHT
		public int gpHeight(String fileLocationsent, String videoNamesent)
		{
			int picHeight;
			BufferedImage imgHght = null;
			
			String fileLocation = fileLocationsent;
			String videoName = videoNamesent;
			
			FFmpegFrameGrabber grab = new FFmpegFrameGrabber(fileLocation + videoName);
			try {	grab.start(); 	} 
			catch (Exception e) {	System.out.println("Unable to grab frames");	}
			
			try {	imgHght = grab.grab().getBufferedImage();	} 
			catch (org.bytedeco.javacv.FrameGrabber.Exception e) {	/*e.printStackTrace();*/ }
				    	
			picHeight = imgHght.getHeight();
					    
			return picHeight;			
		}//END GET PIXELS HEIGHT
}//END CLASS FrameCount